package figure;

public class Trapezoid {
	double firstBase;
	double secondBase;
	double firstLeg;
	double secondLeg;
	double height;
	
	Trapezoid(double firstBase, double secondBase, double firstLeg, double secondLeg, double h) {
		this.firstBase = firstBase;
		this.secondBase = secondBase;
		this.firstLeg = firstLeg;
		this.secondLeg = secondLeg;
		height = h;
	}
	
	public double getPerimeter() {
		return firstBase + secondBase + firstLeg + secondLeg;
	}
	
	public double getArea() {
		return (firstBase + secondBase) / 2 * height;
	}
	
	public void printResult() {
		System.out.printf("%nThe area of trapezoid = %f. %n", getArea());
		System.out.printf("The perimeter of trapezoid = %f.", getPerimeter());
	}
}
