package figure;

public class Triangle {
	double a;
	double b;
	double c;
	
	Triangle(double sideA, double sideB, double sideC) {
		a = sideA;
		b = sideB;
		c = sideC;
	}
	
	public double getPerimeter() {
		return  a + b + c; 
	}
	
	public double getArea() {
		double halfperimeter = 0.5 * (a + b + c);
		return Math.sqrt(halfperimeter * (halfperimeter - a) * (halfperimeter - b) * (halfperimeter - c));
	}	
	
	public void printResult() {
	System.out.printf("%nThe area of triangle = %f. %n", getArea());
	System.out.printf("The perimeter of triangle = %f.", getPerimeter());
	}
}

