package figure;

public class Rectangle {
	double a;
	double b;
	
	Rectangle(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	public double getPerimeter() {
		return 2 * (a + b);
	}
	
	public double getArea() {
		return a * b;
	}
	
	public void printResult() {
		System.out.printf("%nThe area of rectangle = %f. %n", getArea());
		System.out.printf("The perimeter of rectangle = %f.", getPerimeter());
	}
}
