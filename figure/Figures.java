package figure;
import java.util.Arrays;

import figure.Ellipse;
import figure.Triangle;
import figure.Trapezoid;
import figure.Rectangle;

public class Figures {

	public static void main(String[] args) {
		for(int i = 0; i < args.length; i++) {
			System.out.print(Arrays.asList(args[i])); 
		}
		String type = args[0]; 
		switch (type) {
			case "triangle":
				double sideA = Double.parseDouble(args[1]);
				double sideB = Double.parseDouble(args[2]);
				double sideC = Double.parseDouble(args[3]);
				Triangle triangle = new Triangle(sideA, sideB, sideC);
				triangle.printResult();
					break;
			case "rectangle":
				double a = Double.parseDouble(args[1]);
				double b = Double.parseDouble(args[2]);
				Rectangle rectangle = new Rectangle(a, b);
				rectangle.printResult();
					break;
			case "ellipse":
				double halfAxisA = Double.parseDouble(args[1]);
				double halfAxisB = Double.parseDouble(args[2]);
				Ellipse ellipse = new Ellipse(halfAxisA, halfAxisB);
				ellipse.printResult();
					break;
			case "trapezoid":
				double firstBase = Double.parseDouble(args[1]);
				double secondBase = Double.parseDouble(args[2]);
				double firstLeg = Double.parseDouble(args[3]);
				double secondLeg = Double.parseDouble(args[4]);
				double height = Double.parseDouble(args[5]);
				Trapezoid trapezoid = new Trapezoid(firstBase, secondBase, firstLeg, secondLeg, height);
				trapezoid.printResult();
					break;
			default:
				System.out.printf("%nInvalid figure");
					break;
		}
		
		
		
		
	}

}
