package figure;

public class Ellipse {
	double halfAxisA;
	double halfAxisB;
	
	Ellipse (double halfAxisA, double halfAxisB) {
		this.halfAxisA = halfAxisA;
		this.halfAxisB = halfAxisB;
	}
	
	public double getPerimeter() {
		return 4 * (Math.PI * halfAxisA * halfAxisB + (halfAxisA - halfAxisB)) / (halfAxisA + halfAxisB);
	}
	
	public double getArea() {
		return Math.PI * halfAxisA * halfAxisB;
	}
	
	public void printResult() {
		System.out.printf("%nThe area of ellipse = %f. %n", getArea());
		System.out.printf("The perimeter of ellipse = %f.", getPerimeter());
	}
}
